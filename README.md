This is secondary1 server of "replicated log" system.
It runs on :8081 port

To receive all stored messages in secondary1 server, make GET request on localhost:8081/api/replicated-log/v1/message-memory
